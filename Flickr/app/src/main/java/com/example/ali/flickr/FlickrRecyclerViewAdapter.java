package com.example.ali.flickr;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by ali on 8/22/17.
 */

class FlickrRecyclerViewAdapter extends RecyclerView.Adapter<FlickrRecyclerViewAdapter.FlickImageViewHolder> {
    private static final String TAG = "FlickrRecyclerViewAdapt";

    private List<Photo> mPhotoList;
    private Context mContext;

    public FlickrRecyclerViewAdapter(Context context, List<Photo> photoList) {
        Log.d(TAG, "FlickrRecyclerViewAdapter: constroctor clled");
        mPhotoList = photoList;
        mContext = context;
    }

    @Override
    public FlickImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {//just inflate the view
        Log.d(TAG, "onCreateViewHolder: starts");
        //Called by layout manager when it nedds view
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.browse, parent, false);//parent ro hatman bayad bedim chon age nadim va null bedim chizi darbare style ha va theme ha nemifahme in yek mored bood ke bad kar mikkone pas parent ro bede

        return new FlickImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FlickImageViewHolder holder, int position) {//wants new data to be stored in view Holdet to can display
        //Called by layout manager when it wants new data in an existing row (kare in tabe)
        //int position ke dar voroodi has oon item ie has ke mikhad bind she yani mikhad neshoon dade beshe

        if ((mPhotoList == null) || (mPhotoList.size() == 0)) {//age ke search kard va chizi nabood ba photo khali bood biyad in karo kone
            holder.thumbnail.setImageResource(R.drawable.error);
            holder.title.setText(R.string.no_photo);
            holder.title.setTextColor(Color.rgb(255,00,00));
        } else {
            Photo photoItem = mPhotoList.get(position);
            Log.d(TAG, "onBindViewHolder: " + photoItem.getTitle() + " --------> " + position);
            Picasso.with(mContext).load(photoItem.getImage())
                    .error(R.drawable.error)
                    .placeholder(R.drawable.picture)
                    .into(holder.thumbnail);

            holder.title.setText(photoItem.getTitle());
        }

    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "getItemCount: called");
        return ((mPhotoList != null) && (mPhotoList.size() != 0) ? mPhotoList.size() : 1);//1 return mikonim chon mikhayem vaghti ke dar search esh chizi nabood oon error ro neshoon bede
    }

    //albate baraye bare avval ke app run mishe PhotoLIst khali ast ke dar onCreate MainActivity has bad ke miyad onDataAvailable oonja besh migim ke loadNewData() kon
    void loadNewData(List<Photo> newPhotos) {//khob mikhayem vaghti ke query search ma taghir kard adapter ham taghir kone dg
        mPhotoList = newPhotos;
        notifyDataSetChanged();//be recycler migim ke data ha avaz shode va refresh sho
    }

    public Photo getPhoto(int position) {//mikham vaghti ro ye picture too recycler click kard in tabe ro run konam az activity main va bedoonam kodoom image ast
        return ((mPhotoList != null) && (mPhotoList.size() != 0) ? mPhotoList.get(position) : null);
    }


    static class FlickImageViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "FlickImageViewHolder";
        ImageView thumbnail = null;
        TextView title = null;

        public FlickImageViewHolder(View itemView) {
            super(itemView);
            Log.d(TAG, "FlickImageViewHolder: constroctor called");
            this.thumbnail = (ImageView) itemView.findViewById(R.id.thumbnail);
            this.title = (TextView) itemView.findViewById(R.id.title);
        }
    }

}
