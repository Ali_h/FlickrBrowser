package com.example.ali.flickr;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class PhotoDetailActivity extends BaseActivity {
    TextView photoTitle;
    TextView photoTags;
    ImageView photoImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_detail);

        activateToolbar(true);

        Intent intent = getIntent();
        Photo photo = (Photo) intent.getSerializableExtra(PHOTO_TRANSFER);
        if (photo != null){
            photoTitle = (TextView) findViewById(R.id.photo_title);
            photoTags = (TextView) findViewById(R.id.photo_tags);
            photoImage = (ImageView) findViewById(R.id.photo_image);

            Resources resources = getResources();

            photoTitle.setText(resources.getString(R.string.photo_title_fullscreen , photo.getTitle()));
            photoTags.setText(resources.getString(R.string.photo_tags_fullscreen , photo.getTag()));

            Picasso.with(this).load(photo.getFullScreenImage())
                    .error(R.drawable.error)
                    .placeholder(R.drawable.picture)
                    .into(photoImage);
        }
    }

}
