package com.example.ali.flickr;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ali on 8/20/17.
 */

class GetFlickrJsonData extends AsyncTask<String, Void, List<Photo>> implements GetRawData.OnDownloadComplete {
    private static final String TAG = "GetFlickrJsonData";

    private List<Photo> mPhotoList = null;
    private String mBaseURL;//url asli bedoone hich parametri
    private String mLanguge;//languge dar url ro mige
    private boolean mMatchAll;//in ham yek parametre dar url ast

    private boolean runInSameThread = false;

    interface OnDataAvailable {
        void onDataAvailable(List<Photo> data, DownloadStatus status);
    }

    private final OnDataAvailable mCallBack;

    public GetFlickrJsonData(OnDataAvailable callBack, String baseURL, String languge, Boolean matchAll) {
        Log.d(TAG, "GetFlickrJsonData: constructor called");
        mBaseURL = baseURL;
        mLanguge = languge;
        mMatchAll = matchAll;
        mCallBack = callBack;
    }

    @Override
    protected void onPostExecute(List<Photo> photos) {
        Log.d(TAG, "onPostExecute: starts");

        if (mCallBack != null) {
            mCallBack.onDataAvailable(mPhotoList, DownloadStatus.OK);
        }

        Log.d(TAG, "onPostExecute: end");
    }

    @Override
    protected List<Photo> doInBackground(String... params) {
        Log.d(TAG, "doInBackground: starts");
        String destinationUri = createUri(params[0], mLanguge, mMatchAll);//ghable inke berim va getRowData ro farakhni konim ke data ro download kone bayad oon ro create konim

        GetRawData getRawData = new GetRawData(this);//this ro vase callback ferestadeim
        getRawData.runInSameThread(destinationUri);
        Log.d(TAG, "doInBackground: ends");
        return mPhotoList;
    }


    //in methode payin ro dar tozihat goftam ke karo anjam mide vali parse kardanesh dar UI thread ast hatman tozihat ro bekhoon dar shomare 11 goftam
//    void executeOnSameThread(String searchCriteria) {
//        Log.d(TAG, "executeOnSameThread: called");
//        runInSameThread = true;
//        String destinationUri = createUri(searchCriteria, mLanguge, mMatchAll);//ghable inke berim va getRowData ro farakhni konim ke data ro download kone bayad oon ro create konim
//
//        GetRawData getRawData = new GetRawData(this);//this ro vase callback ferestadeim
//        getRawData.execute(destinationUri);
//        Log.d(TAG, "executeOnSameThread: ends");
//    }

    private String createUri(String searchCriteria, String languge, boolean matchAll) {
        Log.d(TAG, "createUri: starts");

//        Uri uri = Uri.parse(mBaseURL);//hamoon tor ke midooni uri mitoone addres yek jaie bashe hala inja url has
//        Uri.Builder builder = uri.buildUpon();//in karo mikonim ke betoonim be tahesh parametre ezafe konim
//        builder = builder.appendQueryParameter()//appendQueryParameter : havasesh has ke vaghti parametre ezafe mikone ? va & ro dar url bezare ke hamishe yek uri dorost bashe pas in ro khodesh handle mikone(hamoon tor ke midooni dar uri ha vase gozashtane parametre jadid az ? va az oon be bad ba & estefade mikonim)
        return Uri.parse(mBaseURL).buildUpon()
                .appendQueryParameter("tags", searchCriteria)
                .appendQueryParameter("tagmode", matchAll ? "ALL" : "ANY")//if matchAll == true matchAll = "ALL" if false matchAll="ANY"
                .appendQueryParameter("lang", languge)
                .appendQueryParameter("format", "json")
                .appendQueryParameter("nojsoncallback", "1")
                .build().toString();//toString chon in method yek string bar migardoone

    }

    @Override
    public void onDownloadComplete(String data, DownloadStatus status) {
        Log.d(TAG, "onDownloadComplete: starts status : " + status);
        if (status == DownloadStatus.OK) {
            mPhotoList = new ArrayList<>();//vase inke data haro begire berized dar yek Photo object va bad oon ro add konim too in
            //alan ma bayad data haye json ro parse konim age error dasht yek json exception mifreste vase hamin in kar haro too try mikonim
            try {
                JSONObject jsonData = new JSONObject(data);//in yek object dar json ro migire
                JSONArray itemsArray = jsonData.getJSONArray("items");//chon chizi ke ma mikhayem dar tage item has ke in item yek json arraye has bebin be har chi beyne {} bashe migan object hala age yek properti dar in object intori bashe [] in mishe yek json arraye dar yek json object
                //pas alan ma kolle item ro darim hatman boro va bebin json che shekli has ma chiz haye ke mikhayem dar arraye item has

                for (int index = 0; index < itemsArray.length(); index++) {
                    JSONObject jsonPhoto = itemsArray.getJSONObject(index);//hala in items arraye khodesh az object haie tashkil shode ke har object yek photo mishe dar vaghe ke intori har object ro migirim
                    //pas dar avval yek object kolli darim bad oomadim arraye item ro gereftim in arraye khodesh moteshakkel az object haye ast ke har kodoom yek photo mishe ke bayad begirimeshoon
                    String title = jsonPhoto.getString("title");//in esma ha ke dadim dar getString hamoon properti haye daroon json ast
                    String author = jsonPhoto.getString("author");
                    String authorId = jsonPhoto.getString("author_id");
                    String tags = jsonPhoto.getString("tags");

                    JSONObject jsonMedia = jsonPhoto.getJSONObject("media");//hala in media khodesh yek object has dar in items boro hatman bebin
                    String photoUrl = jsonMedia.getString("m");

                    String fullScreenImage = photoUrl.replaceFirst("_m.", "_b.");//in mishe oon image ie ke click mishe va oon ro bozorgtar neshoon mide
                    //in fullscreen image dar json nis va khodemoon sakhtim chon dar khode flickr gofte bood vase full screen bayad tahesh _b bashe

                    Photo photoObject = new Photo(title, author, authorId, fullScreenImage, tags, photoUrl);
                    mPhotoList.add(photoObject);

                    Log.d(TAG, "onDownloadComplete: ends parsing json data : " + photoObject.toString());
                }

            } catch (JSONException error) {
                Log.e(TAG, "onDownloadComplete: error in parsing data : " + error.getMessage());
                error.printStackTrace();
                status = DownloadStatus.FAILED_OR_EMPTY;
            }
        }

        //in ham be hamoon dalile executeOnSameThread ghabele ghabool nis dar tozihat goftam
//        if (runInSameThread && mCallBack != null) {//hamishe vaghti callback migirim bayad null check she
//            mCallBack.onDataAvailable(mPhotoList, status);
//            Log.d(TAG, "onDownloadComplete: eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
//        }

        Log.d(TAG, "onDownloadComplete: ends");
    }
}
