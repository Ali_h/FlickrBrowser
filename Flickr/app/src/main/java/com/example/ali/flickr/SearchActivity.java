package com.example.ali.flickr;

import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.widget.SearchView;

public class SearchActivity extends BaseActivity {
    private static final String TAG = "SearchActivity";

    private SearchView mSearchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: starts");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        activateToolbar(true);
        Log.d(TAG, "onCreate: ends");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "onCreateOptionsMenu: starts");
        getMenuInflater().inflate(R.menu.menu_search, menu);
//        or
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.menu_search , menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);//SearchManager provides access to the system search service
        mSearchView = (SearchView) menu.findItem(R.id.app_bar_search).getActionView();//getActionView hamoon actionViewClass ast ke dar menu_search xml tarif kardim
        SearchableInfo searchableInfo = searchManager.getSearchableInfo(getComponentName());//in miyad configure haie ke too searchable.xml ro gozashtim migire
        mSearchView.setSearchableInfo(searchableInfo);

        Log.d(TAG, "onCreateOptionsMenu: " + getComponentName().toString());
        Log.d(TAG, "onCreateOptionsMenu: hint is " + mSearchView.getQueryHint());
        Log.d(TAG, "onCreateOptionsMenu: searchable info is " + searchableInfo.toString());

        mSearchView.setIconified(false);//age in true bashe vaghti too activity main roo search click mikonim mire be searchActivity ta injas dorost vali dobare too searchActivity bayad roo icon search click kard
        //ta betooni search ro bebini hala age false bashe in kar nemikhad

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {//zamani ke user search ro submit mikone
                Log.d(TAG, "onQueryTextSubmit: called");
                //vase inke search ma kar kone bayad in query ro pas bedim be mainActivity ta oonja execute beshe dg khob hala vase inke ma betoonim query ro bedim be mainActivity ye chizi has be esme
                //sharedpreferences ke inkaro vase ma mikone va mese bundle ham has key value ast dar vaghe mishe goft ke mese ye database mimoone va data haye app ro dar khodesh zakhire mikone
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());//in nahve gereftane yek shared ast . khob ma dar akhar vase context be jaye inke this bedim getApplicationContext(context khode aplication) dadim chon mikhad dar ye activity dg ejra she
                sharedPreferences.edit().putString(FLICKR_QUERY , query).apply();//khob mese bundle has value ke query has ro be onvane key FLICKR_QUERY midi besh hamin va hatman ham bayad ghable finish bashad ke activity avaz nashode
                //vaghti apply() ro mizani data save mishe
                //hala mitooni az mainActivity be oon dastresi dashte bashe
                mSearchView.clearFocus();//Called when this view wants to give up focus.
                //khob ma ye moshkel dashtim oonam in ke vaghti search mikardim ba in ke finish dasht be main barmigasht vali hanoo dasht search ro neshoon midad khodesh too 163 gof be khatere ine ke az keybord extenal estefade mikonim
                //vali ba khode gooshi oke chon keyborde khodeshe hala vaghti clearFocus mikonim dorost mishe
                finish();//Call this when your activity is done and should be closed. The ActivityResult is propagated back to whoever launched you via onActivityResult().
                //khob ma ham mikhayem vaghti ke serach kard bargarde be mainActivity ta image ha ro bebine dar vaghe in finish baes mishe ke activity hazer baste she va bere be activity ke oon ro baz karde
                return true;//true if the query has been handled by the listener, false to let the SearchView perform the default action.
            }

            @Override
            public boolean onQueryTextChange(String s) {
                Log.d(TAG, "onQueryTextChange: called");
                return false;
            }
        });

        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                finish();
                return false;//true if the listener wants to override the default behavior of clearing the text field and dismissing it, false otherwise
            }
        });

        return true;
    }


}
