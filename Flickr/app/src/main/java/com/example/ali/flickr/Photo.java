package com.example.ali.flickr;

import java.io.Serializable;

/**
 * Created by ali on 8/20/17.
 */

class Photo implements Serializable{//in vase ine ke too activity main vase inke too intent.putextra mikhayem photo befrestim error mide
    //bayad data ha ro Serializable konim va chon har mashin virtual java khodesh ro dare bayad barash ye uid khas bezarim vagar na khode java in kar mikone
    private static final long serialVersionUID= 1L;//L yani long
    //pas in uid ro vase in gozashtim ke khode virtual mashin java vase ma hey yek uid nazare chon momkene moshkel be vojood biyad

    private String mTitle;
    private String mAuthor;
    private String mAuthorId;
    private String mFullScreenImage;
    private String mTag;
    private String mImage;

    public Photo(String title, String author, String authorId, String fullScreenImage, String tag, String image) {
        mTitle = title;
        mAuthor = author;
        mAuthorId = authorId;
        mFullScreenImage = fullScreenImage;
        mTag = tag;
        mImage = image;
    }

    String getTitle() {
        return mTitle;
    }

    String getAuthor() {
        return mAuthor;
    }

    String getAuthorId() {
        return mAuthorId;
    }

    String getFullScreenImage() {
        return mFullScreenImage;
    }

    String getTag() {
        return mTag;
    }

    String getImage() {
        return mImage;
    }

    @Override
    public String toString() {
        return  "mTitle = " + mTitle + '\n' +
                ", mAuthor = " + mAuthor + '\n' +
                ", mAuthorId = " + mAuthorId + '\n' +
                ", mLink = " + mFullScreenImage + '\n' +
                ", mTag = " + mTag + '\n' +
                ", mImage = " + mImage + '\n';
    }
}
