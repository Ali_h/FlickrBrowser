package com.example.ali.flickr;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

/**
 * Created by ali on 9/3/17.
 */

public class BaseActivity extends AppCompatActivity {
    private static final String TAG = "BaseActivity";
    static final String FLICKR_QUERY = "FLICKR_QUERY";
    static final String PHOTO_TRANSFER = "PHOTO_TRANSFER";

    void activateToolbar(boolean enableHome) {
        Log.d(TAG, "activateToolbar: starts");
        ActionBar actionBar = getSupportActionBar();//hatman male library v7 bashe ke compat bashe
        if (actionBar == null){
            Log.d(TAG, "activateToolbar: action bar null");
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            if (toolbar != null){
                setSupportActionBar(toolbar);//Set a Toolbar to act as the ActionBar for this Activity window.
                //dar vaghe in khat dar hame activity ha has oon avvalesh bade setCOntentView
                actionBar = getSupportActionBar();//inja baz action bar ro getSupportActionBar() mikonim ke dg null nabashe va varede if badi she
            }
        }
        //chera inja else nazashtim chon dar bala baz action bar ro getSupportActionBar() mikonim ke biyad varede in if she ta barash home button bezarim
        if (actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(enableHome);
        }
    }
}
