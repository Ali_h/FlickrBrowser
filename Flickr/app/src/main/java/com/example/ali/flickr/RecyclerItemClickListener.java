package com.example.ali.flickr;

import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by ali on 9/3/17.
 */

class RecyclerItemClickListener extends RecyclerView.SimpleOnItemTouchListener {
    private static final String TAG = "RecyclerItemClickListen";

    interface OnRecyclerClickListener {
        void onItemClick(View view, int position);

        void onItemLongClick(View view, int position);
    }

    private final OnRecyclerClickListener mListener;//for interface callback
    private final GestureDetectorCompat mGestureDetectorCompat;

    public RecyclerItemClickListener(Context context, final RecyclerView recyclerView, OnRecyclerClickListener listener) {
        mListener = listener;
        mGestureDetectorCompat = new GestureDetectorCompat(context ,new GestureDetector.SimpleOnGestureListener(){
            @Override
            public boolean onSingleTapUp(MotionEvent e) {//khob chize asli injas childView : in miyad mige ke findChildViewUnder(e.getX() , e.getY()) dar oonja goftam ke
                //e chiz haye mohemi toosh has az jomle moghe iyate x o y click ma hala ma besh in do ta ro midim va in mire oon view ie ke zire in moghe iyat has ro be ma mide age chizi bood ke ye view mishe
                //age chizi nabood masalan khali bood vali tavajoh kon ke bayad daroon recycler bashe pas age khali bood null mishe pas age child view (ecyclerView.findChildViewUnder(e.getX() , e.getY()))
                //null bood yani hichi ziresh nis va bakhshe khali recycler ast pas hamishe bayad null check ro anjam bedi
                Log.d(TAG, "onSingleTapUp: starts");
                View childView = recyclerView.findChildViewUnder(e.getX() , e.getY());
                if (childView != null && mListener != null){//inja ham besh migim ke age childView != null yani khali nabood biya ye kari kon chon bala goftam momkene ke khali bashe dar recycler
                    Log.d(TAG, "onSingleTapUp: calling listener.onItemClick");
                    mListener.onItemClick(childView , recyclerView.getChildAdapterPosition(childView));//getChildAdapterPosition(View) miyad position oon object ro mide ke khoone chandome
                }
                return true;
            }

            @Override
            public void onLongPress(MotionEvent e) {//in method va yek method dg return boolean nadaran
//                super.onLongPress(e);
                Log.d(TAG, "onLongPress: starts");
                View childView = recyclerView.findChildViewUnder(e.getX() , e.getY());
                if (childView != null && mListener != null){
                    Log.d(TAG, "onLongPress: calling listener.onITemlongclick");
                    mListener.onItemLongClick(childView , recyclerView.getChildAdapterPosition(childView));
                }
            }
        });
    }

    //in tebe hatman bayad bashe chon age nabashe aslan kar nemide click kardan
    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {//khob dar in method ma GestureDetectorCompat ro vared kar mikonim ta oon vasamoon handle kone ke che no gesture vared shode
        //in e ke be onvane voroodi has tamame chiz haro rajebe click ma roo image mige masalan mogheiyate x o y va hame chi
        Log.d(TAG, "onInterceptTouchEvent: starts");
        if (mGestureDetectorCompat != null) {//inja theory in ast ke har chi ke gesture detector betoone az pasesh bar biyad true return mikone va har chi natoone false
            boolean result = mGestureDetectorCompat.onTouchEvent(e);
            Log.d(TAG, "onInterceptTouchEvent: returned --> " + result);
            return result;
        } else {
            Log.d(TAG, "onInterceptTouchEvent: returend false");
            return false;
        }
    }
}
