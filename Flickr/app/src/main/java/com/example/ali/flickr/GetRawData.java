package com.example.ali.flickr;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

enum DownloadStatus {IDLE, PROCESSING, NOT_INITIALISED, FAILED_OR_EMPTY, OK} //vaziate download ma ro negah midare

/**
 * Created by ali on 8/20/17.
 */

class GetRawData extends AsyncTask<String, Void, String> {//hatta mishe in class ro az async bardasht chon ke ma in class ro ke execute nemikonim dar vaghe hich kari nemikone va faghat ye tabe barash bezarim ke data ro down kone
    private static final String TAG = "GetRawData";

    private DownloadStatus mDownloadStatus;

    interface OnDownloadComplete {
        void onDownloadComplete(String data , DownloadStatus status);
    }
    private OnDownloadComplete callback;

    public GetRawData(OnDownloadComplete callback) {
        Log.d(TAG, "GetRawData: constructor called");
        this.mDownloadStatus = DownloadStatus.IDLE;//not processing anything
        this.callback = callback;
    }

    void runInSameThread(String s){
        Log.d(TAG, "runInSameThread: starts");

        if (callback != null){
            String resualt = doInBackground(s);
            callback.onDownloadComplete(resualt , mDownloadStatus);
            //onPostExecute(doInBackground(s) , mDownloadStatus) // inro ham mishe nevesht vali khob niyaz nis chon dar vaghe in class dorost ast ke async extend karde vali be soorate kolli async nis
            //balke dar hamoon thread JsonData anjam mishe
            //balke dar hamoon thread JsonData anjam mishe
        }

        Log.d(TAG, "runInSameThread: ends");

    }

    //mitoonim azesh dar bala estefade konim vali niyazi nis va be hamoon dalile executeOnsameThread in ro comment kardim
//    @Override
//    protected void onPostExecute(String data) {
//        Log.d(TAG, "onPostExecute: starts with : " + data);
////        super.onPostExecute(data);
//        if (callback != null){
//            callback.onDownloadComplete(data , mDownloadStatus);
//        }
//    }

    @Override
    protected String doInBackground(String... strings) {
        Log.d(TAG, "doInBackground: starts");
        HttpURLConnection connection = null;
        BufferedReader reader = null;

        if (strings == null) {
            mDownloadStatus = DownloadStatus.NOT_INITIALISED;
            return null;
        }

        try {
            mDownloadStatus = DownloadStatus.PROCESSING;
            URL url = new URL(strings[0]);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            int responseCode = connection.getResponseCode();
            Log.d(TAG, "doInBackground: response code : " + responseCode);

            StringBuilder resualt = new StringBuilder();
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String line;
            while (null != (line = reader.readLine())) {
                resualt.append(line).append("\n");
            }


            mDownloadStatus = DownloadStatus.OK;
            return resualt.toString();


        } catch (MalformedURLException error) {
            Log.e(TAG, "doInBackground: invalid url" + error.getMessage());
        } catch (IOException error) {
            Log.e(TAG, "doInBackground: IOException" + error.getMessage());
        } catch (SecurityException error) {
            Log.e(TAG, "doInBackground: securtiy exectptoin needs permission ? " + error.getMessage());
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException error) {
                    Log.e(TAG, "doInBackground: error closing stream" + error.getMessage());
                }
            }
        }

        mDownloadStatus = DownloadStatus.FAILED_OR_EMPTY;
        return null;
    }
}
